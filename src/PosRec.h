#pragma once

#include "ofxGrt.h"
#include "ofMain.h"
#include "globalvars.h"

class PosRec {
    public:
        PosRec();
        void setup(const vector<size_t> mbIDs, const string address);
        void update();
        void draw();
        void keyPressed(int key);
        void setRecord(bool r);
        void setTrained();
        void setLabel(UINT label);

    private:
        ClassificationData trainingData;
        GestureRecognitionPipeline pipeline;
        bool record = false;
        bool drawInfo = true;
        UINT trainingClassLabel;
        string infoText;
        ofTrueTypeFont font;
        ofxGrtTimeseriesPlot accDataPlot;
        ofxGrtTimeseriesPlot predictionPlot;

        GRT::VectorFloat acc;

        vector<size_t> mbIDs;
        string address;
        int numDimensions;
        int prevPose;
        const int numPoses = 8;
        int index = -1;
};
