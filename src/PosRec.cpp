#include "PosRec.h"

PosRec::PosRec(){ }

void PosRec::setup(const vector<size_t> mbIDs_, string address_){

    mbIDs = mbIDs_;
    address = address_;

    font.load("verdana.tff", 12, true, true);
    font.setLineHeight(12.0f);

    infoText = "";
    trainingClassLabel = 1;

    size_t mNumMBs = mbIDs.size();
    numDimensions = mNumMBs * 3;
    acc.resize(numDimensions);
    trainingData.setNumDimensions( numDimensions );

    prevPose = -1;

    ANBC naiveBayes;
    naiveBayes.enableNullRejection(false);
    naiveBayes.setNullRejectionCoeff(3);
    pipeline.setClassifier(naiveBayes);

    accDataPlot.setup( 500, numDimensions, "acc");
    accDataPlot.setDrawGrid(true);
    accDataPlot.setFont( font );
}

void PosRec::update() {

    for (size_t i = 0; i<mbIDs.size(); i++) {
        size_t idx = mbIDs[i];
        acc[i*3+0] = mbData.getMiniBee(idx).getX();
        acc[i*3+1] = mbData.getMiniBee(idx).getY();
        acc[i*3+2] = mbData.getMiniBee(idx).getZ();
    };

    GRT::VectorFloat prediction;

    accDataPlot.update(acc);

    if(record) {
        trainingData.addSample(trainingClassLabel, acc);
    }

    if(pipeline.getTrained()) {
        pipeline.predict(acc);
        prediction = pipeline.getClassLikelihoods();
        predictionPlot.update(prediction);
        int sum = 0;
        index = -1;
        for(size_t i=0; i<prediction.size(); i++){
            sum += prediction[i];
            if(prediction[i]==1){
                index = ofClamp(i, -1, numPoses);
            }
        }
        ofxOscMessage msg;
        if ((sum>0)&&(prevPose!=index)){
            msg.setAddress(address);
            msg.addIntArg(index);
            oscSender.sendMessage(msg);
            prevPose = index;
        }
    }
}

void PosRec::draw() {

    int marginX = ofGetWidth() * 0.8;
    int marginY = 20;
    // int graphX = marginX;
    // int graphY = ofGetHeight() / 2;
    // int graphW = ofGetWidth() * 0.2;
    // int graphH = ofGetHeight() * 0.2;

    if( drawInfo ){
        float infoX = marginX;
        float infoW = 250;
        float textX = marginX;
        float textY = marginY;
        float textSpacer = font.getLineHeight() * 1.5;

        ofFill();
        ofSetColor(10,10,10);
        ofDrawRectangle( infoX, 5, infoW, 225 );
        ofSetColor( 255, 255, 255 );

        font.drawString( "[i]: Toogle Info", textX, textY ); textY += textSpacer;
        font.drawString( "[r]: Toggle Recording", textX, textY ); textY += textSpacer;
        font.drawString( "[t]: Train Model", textX, textY ); textY += textSpacer;
        font.drawString( "[1..8]: Set Class Label", textX, textY ); textY += textSpacer;

        textY += textSpacer;
        font.drawString( "Class Label: " + ofToString( trainingClassLabel ), textX, textY ); textY += textSpacer;
        font.drawString( "Recording: " + ofToString( record ), textX, textY ); textY += textSpacer;
        font.drawString( "Num Samples: " + ofToString( trainingData.getNumSamples() ), textX, textY ); textY += textSpacer;
        font.drawString( infoText, textX, textY ); textY += textSpacer;

        ////Update the graph position
        // graphX = infoX + infoW + 15;
        // graphW = mainFbo.getWidth() - graphX - 15;
        if ( pipeline.getTrained()){
            textY += textSpacer;
            font.drawString( ofToString(index), textX, textY);
        }
    }

    ////Draw the data graph
    //accDataPlot.draw( graphX, graphY, graphW, graphH ); graphY += graphH * 1.1;

    //////If the model has been trained, then draw the texture
    //if( pipeline.getTrained() ){
    //    predictionPlot.draw( graphX, graphY, graphW, graphH ); graphY += graphH * 1.1;
    // }
}


void PosRec::keyPressed(int key){

    infoText = "";

    switch ( key) {
        case 'r':
            record = !record;
            break;
        case '1':
            trainingClassLabel = 1;
            break;
        case '2':
            trainingClassLabel = 2;
            break;
        case '3':
            trainingClassLabel = 3;
            break;
        case '4':
            trainingClassLabel = 4;
            break;
        case '5':
            trainingClassLabel = 5;
            break;
        case '6':
            trainingClassLabel = 6;
            break;
        case '7':
            trainingClassLabel = 7;
            break;
        case '8':
            trainingClassLabel = 8;
            break;
        case 't':
            setTrained();
            break;
        case 's':
            if( trainingData.save( ofToDataPath("TrainingData.grt") ) ){
                infoText = "Training data saved to file";
            }else infoText = "WARNING: Failed to save training data to file";
            break;
        case 'l':
            if( trainingData.load( ofToDataPath("TrainingData.grt") ) ){
                infoText = "Training data saved to file";
            }else infoText = "WARNING: Failed to load training data from file";
            break;
        case 'c':
            trainingData.clear();
            infoText = "Training data cleared";
            break;
        case 'i':
            drawInfo = !drawInfo;
            break;
        default:
            break;
    }
}

void PosRec::setRecord(bool r){ record = r; }
void PosRec::setTrained(){ 
            if( pipeline.train( trainingData ) ){
                infoText = "Pipeline Trained";
                predictionPlot.setup( 500, pipeline.getNumClasses(), "prediction likelihoods" );
                predictionPlot.setDrawGrid( true );
                predictionPlot.setDrawInfoText( true );
                predictionPlot.setFont( font );
            }else infoText = "WARNING: Failed to train pipeline";
}
void PosRec::setLabel(UINT l){ trainingClassLabel = l; }
