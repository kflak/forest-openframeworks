#pragma once

#include "ofMain.h"
#include "sensor/minibee/MiniBeeData.h"

extern MiniBeeData mbData;
extern const int numMBs;
extern ofFbo mainFbo;

extern ofxOscSender oscSender;
