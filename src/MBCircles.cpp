#include "MBCircles.h"

MBCircles::MBCircles() {
}
void MBCircles::setup(){

    theta = 2 * PI / 8 ;
    thetaInc = 2*PI / numMBsInCircle;

    ofPushMatrix();
    ofTranslate(mainFbo.getWidth() / 2.0, mainFbo.getHeight() / 2.0); 
    for(int i = 0; i < numMBs; i++){

        float x = radius * cos(theta);
        float y = radius * sin(theta);
        theta += thetaInc;

        MBCircle m;
        mbCircle.push_back(m);

        m.setup(x, y, 100);
    }
    ofPopMatrix();

}
void MBCircles::update(){

}

void MBCircles::draw(){


    ofPushMatrix();
    ofTranslate(mainFbo.getWidth() / 2.0, mainFbo.getHeight() / 2.0); 
    ofClear(0);
    theta = 2 * PI / 8 ;

    float delta;
    for(int i = 0; i < numMBs; i++){
        delta = mbData.getMiniBee(i).getDelta();
        mbCircle[i].setRadius(delta * deltaMul);
        mbCircle[i].setAlpha(ofMap(delta, 0.0, 1.0, 200, 255));
        mbCircle[i].setR(ofMap(delta, 0.0, 1.0, 150, 255));
        mbCircle[i].setB(ofMap(delta, 0.0, 1.0, 150, 255));
    }

    for(int i = 0; i < numMBs; i++){

        int multiplier =  i / numMBsInCircle + 1;
        int r = radius * multiplier;
        float t = theta + (multiplier * 2 * PI / 8);
        float x = r * cos(t);
        float y = r * sin(t);
        theta += thetaInc;

        mbCircle[i].setX(x);
        mbCircle[i].setY(y);
        mbCircle[i].draw();
    }
    ofPushMatrix();
}
