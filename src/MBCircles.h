#pragma once

#include "MiniBeeData.h"
#include "ofMain.h"
#include "MBCircle.h"
#include "globalvars.h"

class MBCircles {
    public:
        MBCircles();
        void setup();
        void update();
        void draw();
            
    private:

        vector<MBCircle> mbCircle;
        float deltaMul = 1000.0;
        float theta = 0;
        float thetaInc;
        const int radius = 150;
        const int numMBsInCircle = 4;

};
