#pragma once

#include "ofMain.h"

class MBCircle {
    public:
        MBCircle();
        void setup(float x, float y, float radius);
        void draw();

        inline float getX() const { return x; }
        inline float getY() const { return y; }
        inline float getRadius() const { return radius; }

        void setX(float x = 0.0);
        void setY(float y = 0.0);
        void setRadius(float radius = 1.0);
        void setRgb(float r, float g, float b, float a);
        void setHsb(float h, float s, float b, float a);
        void setR(float r);
        void setG(float g);
        void setB(float b);
        void setAlpha(float a);

        float smoothingFactor = 0.9;
 
    private:
        float x;
        float y;
        float radius;
        float prevRadius;
        float r = 125;
        float g = 100;
        float b = 255;
        float a = 125;
        float prevR = r;
        float prevG = g;
        float prevB = b;
        float prevA = a;
        ofColor color;
};
