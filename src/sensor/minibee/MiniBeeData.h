#pragma once

#include "ofMain.h"
#include "ofxOsc.h"
#include "ofxOscReceiver.h"
#include "ofxGrt.h"
#include "Configuration.h"
#include "MiniBee.h"

class MiniBeeData {

    public:
        MiniBeeData();

        void setup();
        void update();

        void setDeltaAlpha(float alpha);
        void setXAlpha(float alpha);
        void setYAlpha(float alpha);
        float getDeltaAlpha();
        float getXAlpha();
        float getYAlpha();
        GRT::VectorFloat getAcc();

        bool isConnected(int idx);

        MiniBee& getMiniBee(int idx);

        const int port = 12345;
        vector <MiniBee> minibee;
        vector <MiniBee> prevValues;
        const int idOffset = 9;
        const int numMBs = 8;
        const uint64_t timeOut = 1000;

    private:
        ofxOscReceiver receiver;
        float calcDelta(vec3 current, vec3 previous);
        float calcSmooth(float current, float previous, float alpha);
        float deltaAlpha = 0.0;
        float xAlpha = 0.0;
        float yAlpha = 0.0;
        float zAlpha = 0.0;
        vector<uint64_t> t0;
        GRT::VectorFloat acc;
};
