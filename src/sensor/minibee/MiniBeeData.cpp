#include "MiniBeeData.h"

MiniBeeData::MiniBeeData() {
}

void MiniBeeData::setup() {
	// port = 12345;
	for (int i = 0; i < numMBs; i++) {
		MiniBee mb;
		minibee.push_back(mb);
		prevValues.push_back(mb);
		uint64_t t = ofGetElapsedTimeMillis();
		t0.push_back(t);
	}

	for (auto i : minibee) {
		i.setup();
	}
	for (auto i : prevValues) {
		i.setup();
	}

	receiver.setup(port);
	deltaAlpha = 0.9999;
	xAlpha = 0.9999;
	yAlpha = 0.9999;
	zAlpha = 0.9999;

    acc.resize( numMBs * 3, 0 );
}

void MiniBeeData::update() {
	while (receiver.hasWaitingMessages()) {
		ofxOscMessage m;
		receiver.getNextMessage(m);

//		string address = ofToString("/minibee/data");
		if (m.getAddress() == "/minibee/data" && m.getNumArgs() == 4) {
			int id = m.getArgAsInt(0);
			int idx = id - idOffset;

			if(idx >= 0 && idx < numMBs) {
				float x = m.getArgAsFloat(1);
				float y = m.getArgAsFloat(2);
				float z = m.getArgAsFloat(3);

				minibee[idx].setX(x);
				minibee[idx].setY(y);
				minibee[idx].setZ(z);

				// copy over to vectors
				vec3 current, prev;
				current.x = minibee[idx].getX();
				current.y = minibee[idx].getY();
				current.z = minibee[idx].getZ();
				prev.x = prevValues[idx].getX();
				prev.y = prevValues[idx].getY();
				prev.z = prevValues[idx].getZ();

				float prevDelta = prevValues[idx].getDelta();

				// stash away the data for next round of calculations
				prevValues[idx].setX(x);
				prevValues[idx].setY(y);
				prevValues[idx].setZ(z);

				// set the timestamp
				t0[idx] = ofGetElapsedTimeMillis();

				float delta = this->calcDelta(current, prev);
				minibee[idx].setDelta(delta);
				prevValues[idx].setDelta(delta);

				float deltaSmooth = this->calcSmooth(delta, prevDelta, deltaAlpha);
				minibee[idx].setDeltaSmooth(deltaSmooth);

				float xSmooth = this->calcSmooth(x, prev.x, xAlpha);
				minibee[idx].setXSmooth(xSmooth);

				float ySmooth = this->calcSmooth(y, prev.y, yAlpha);
				minibee[idx].setYSmooth(ySmooth);

				float zSmooth = this->calcSmooth(z, prev.z, zAlpha);
				minibee[idx].setZSmooth(zSmooth);
			}
		}
	};
}

MiniBee& MiniBeeData::getMiniBee(int idx) {
	if(idx >= 0 && idx < numMBs)
		return minibee[idx];
	else {
        ofLog(OF_LOG_FATAL_ERROR) << "MiniBeeData::getMiniBee Index " << idx << " out of range. Must be 0 to " << (numMBs-1);
		static MiniBee dummy;
		return dummy;
	}
}

GRT::VectorFloat MiniBeeData::getAcc() {
    for (int i = 0; i < numMBs; i++){
        acc[i*3] = getMiniBee(i).getX();
        acc[i*3+1] = getMiniBee(i).getY();
        acc[i*3+2] = getMiniBee(i).getZ();
    }
    return acc;
}

float MiniBeeData::calcDelta(vec3 current, vec3 previous) {

	vec3 deltas;
	deltas = current - previous;
	deltas = abs(deltas);

	float delta = 0;

	for (int i = 0; i < 3; i++) {
		delta += deltas[i];
	}
	;

	delta = ofClamp(delta / 3, 0.0, 1.0);

	return delta;
}

float MiniBeeData::calcSmooth(float current, float previous, float alpha) {

	return current + alpha * (previous - current);

}

bool MiniBeeData::isConnected(int idx) {
	if(idx >= 0 && idx < numMBs)
		return ofGetElapsedTimeMillis() - t0[idx] < timeOut;
	else {
        ofLog(OF_LOG_FATAL_ERROR) << "MiniBeeData::isConnected Index " << idx << " out of range. Must be 0 to " << (numMBs-1);
		return false;
	}
}
