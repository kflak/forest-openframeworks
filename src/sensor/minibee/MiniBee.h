#pragma once

#include "ofMain.h"
#include "Configuration.h"

using namespace glm;

class MiniBee {
    public:
        MiniBee();
        void setup();

        inline float getX() const { return data.x; }
        inline float getY() const { return data.y; }
        inline float getZ() const { return data.z; }
        inline float getXSmooth() const { return dataSmooth.x; }
        inline float getYSmooth() const { return dataSmooth.y; }
        inline float getZSmooth() const { return dataSmooth.z; }
        inline float getDelta() const { return delta; }
        inline float getDeltaSmooth() const { return deltaSmooth; }

        void setX(float in = 0.0);
        void setXSmooth(float in = 0.0);
        void setY(float in = 0.0);
        void setYSmooth(float in = 0.0);
        void setZ(float in = 0.0);
        void setZSmooth(float in = 0.0);
        void setDelta(float in = 0.0);
        void setDeltaSmooth(float in = 0.0);

    private:
        vec3  data = {0.0, 0.0, 0.0};
        vec3  dataSmooth = {0.0, 0.0, 0.0};
        float delta = 0.0;
        float deltaSmooth = 0.0;
}; 
