#include "MBCircle.h"
#include "ofGraphics.h"

MBCircle::MBCircle() {
}

void MBCircle::setup(float x_, float y_, float radius_){
    x = x_;
    y = y_;
    radius = radius_;
    prevRadius = radius;
    color.setHsb(184, 184, 164, 180);
}

void MBCircle::draw(){
    // ofSetColor(color);
    ofSetColor(r, g, b, a);
    ofDrawCircle(x, y, radius);
}


void MBCircle::setX(float x_) { x = x_; }
void MBCircle::setY(float y_) { y = y_; }
void MBCircle::setRadius(float radius_){ 
    radius = radius_ + smoothingFactor * (prevRadius - radius_);
    prevRadius = radius;
}

void MBCircle::setRgb(float r, float g, float b, float a){
    color.set(r, g, b, a);
}

void MBCircle::setHsb(float h, float s, float b, float a){
    color.setHsb(h, s, b, a);
}

void MBCircle::setR(float r_){ 
    r = r_ + smoothingFactor * (prevR - r_);
    prevR = r;
}
void MBCircle::setG(float g_){ 
    g = g_ + smoothingFactor * (prevG - g_);
    prevG = g;
}
void MBCircle::setB(float b_){ 
    b = b_ + smoothingFactor * (prevB - b_);
    prevB = b;
}
void MBCircle::setAlpha(float a_){ 
    a = a_ + smoothingFactor * (prevA - a_);
    prevA = a;
}

