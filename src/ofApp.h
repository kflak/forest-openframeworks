#pragma once

#include "MBCircles.h"
#include "PosRec.h"
#include "globalvars.h"
#include "ofMain.h"
#include "ofxMidi.h"
#include "ofxOsc.h"
#include "sensor/minibee/MiniBeeData.h"

enum ActivePose{
    KLLI,
    KENNETH
};

class ofApp : public ofBaseApp, public ofxMidiListener{

	public:
		void setup();
		void update();
		void draw();
        void exit();

        void newMidiMessage(ofxMidiMessage& eventArgs);
		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
		
        const int port = 12346;
        // the max that can be sent seems to be 57*57*4
        const int downSampledWidth = 5;
        const int downSampledHeight = 6;
        unsigned char* downsampled;

        // const string videoIP = "192.168.1.103";
        const string videoIP = "25.1.107.255";
        const int videoPort = 7000;

        const int numMBs = mbData.numMBs;

        ofBuffer buffer;

        MBCircles mbCircles;

        PosRec posRec;
        int activePose;
        int label = 1;
        int numPoses = 4;

        ofxMidiIn midiIn;
};
