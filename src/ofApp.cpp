#include "ofApp.h"


//--------------------------------------------------------------
void ofApp::setup(){

    ofSetFrameRate(30);

    ofBackground(0);

    mainFbo.allocate(1000, 1000);

    mbData.setup();

    mbCircles.setup();

    oscSender.setup(videoIP, videoPort);

    const vector<size_t> wrists{ 0, 1, 4, 5 };
    const string address = "/posrec";
    posRec.setup(wrists, address);
    ofxOscMessage m;
    m.setAddress("/posrec");
    m.addInt32Arg(-1);
    oscSender.sendMessage(m);

    midiIn.listInPorts();
	midiIn.openPort("Launch Control:Launch Control MIDI 1 24:0");	// by name
    midiIn.addListener(this);
}

//--------------------------------------------------------------
void ofApp::update(){

    mbData.update();

    posRec.update();
}

//--------------------------------------------------------------
void ofApp::draw(){

    mainFbo.begin();

    mbCircles.draw();

    mainFbo.end();

    ofPixels pixels;

    mainFbo.readToPixels(pixels);
    pixels.resize(downSampledWidth, downSampledHeight);


    if(ofGetFrameNum() % 15 == 0){
        ofxOscMessage m;
        m.setAddress("/fbo");
        for(size_t i = 0; i < pixels.size(); i++){
            m.addCharArg(pixels[i]);
        }
        oscSender.sendMessage(m);
    }

    mainFbo.draw(0, 0);

    posRec.draw();

}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    if(key=='0') posRec.setTrained();

    posRec.keyPressed(key); 

}

void ofApp::exit(){
    ofLog() << "Ciao, my friend!";
	midiIn.closePort();
	midiIn.removeListener(this);
}

//--------------------------------------------------------------
void ofApp::newMidiMessage(ofxMidiMessage &msg){
    if(msg.status < MIDI_SYSEX && label <= numPoses) {
        if(msg.status == MIDI_NOTE_ON){
            posRec.setRecord(true);
        }
        else if (msg.status == MIDI_NOTE_OFF){
            posRec.setRecord(false);
            if (label < numPoses){
                label++;
                posRec.setLabel(label);
            } else {
                posRec.setTrained();
            }
        }
    }
};

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}
